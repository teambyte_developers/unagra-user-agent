/**
 * @author    Israel Castillo  <icastillog@liverpool.com.mx>
 * @copyright Copyright (c) 2018
 * @license   AGPL-3.0
 */

const geoip = require('geo-from-ip')
const extIP = require('external-ip');


let getIP = extIP({
    replace: true,
    services: ['https://ipinfo.io/ip', 'http://ifconfig.co/x-real-ip', 'http://ifconfig.io/ip'],
    timeout: 600,
    getIP: 'parallel',
    userAgent: 'Chrome 15.0.874 / Mac OS X 10.8.1'
});
 


const UserAgent = {
    GetInfo: async(req, res, next) => {
        try {
                const forwarded = req.headers['x-forwarded-for']
                const ip = forwarded ? forwarded.split(/, /)[0] : req.connection.remoteAddress
                
                return res.status(200).json({ip: ip})
            

     
        } catch (e) {
            log
            return res.status(500).json({
                success: false,
                statusCode: 500,
                error: { path: 'InternalErrorServer', message: 'Oops, Something went wrong.' }
            }); 
            
     
        }
    }
}

module.exports = UserAgent