/**
 * @author    Israel Castillo  <icastillog@liverpool.com.mx>
 * @copyright Copyright (c) 2018
 * @license   AGPL-3.0
 */

const bodyParser = require('body-parser') 
const logger = require('morgan') 
const pkg = require('./package') 
const useragent = require('express-useragent')
const express = require('express') 
const app = express()
const dotenv = require('dotenv') 
dotenv.config()

app.use(useragent.express());
app.use(logger('dev'));
app.set('trust proxy', true);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  res.header('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS');
  next();
});

const routes = require('./routes')
app.use('/api/v1', routes);

app.use((err, res) => res.status(404).json({ success: false, message: 'Resource not found' }));

const banner = `
*********************************************************************************************
*
* @version ${pkg.version}
* @Author ${pkg.author}
* @Company Unagra
*
* 🚀  Node Api Running on Port ${process.env.USER_AGENT} 
*********************************************************************************************`;
console.debug(banner);

app.listen(process.env.PORT)
module.exports = app;