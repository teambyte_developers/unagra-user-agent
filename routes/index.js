/**
 * @author    Israel Castillo  <icastillog@liverpool.com.mx>
 * @copyright Copyright (c) 2018
 * @license   AGPL-3.0
 */

const express = require('express')
const router = express.Router()


const prefix = '/getInfo';

const UserAgent = require('../controllers')

router.get(
    `${prefix}`, 
    UserAgent.GetInfo
);

module.exports = router